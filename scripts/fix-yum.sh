#!/bin/sh -x

# Set yum url to vault.centos.org
sudo sed -ri 's|#?baseurl=http://mirror.centos.org|baseurl=https://vault.centos.org|' /etc/yum.repos.d/*
sudo yum update

