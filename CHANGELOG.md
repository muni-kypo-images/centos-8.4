# Changelog

## [qemu-0.2.0], [vbox-0.2.0] - 2022-02-01
### Fixed
- URL of package repositories, as CentOS 8 has reached [EOL](https://www.centos.org/centos-linux-eol/)

## [qemu-0.1.1] - 2022-01-29
### Fixed
- Hostname is added on boot to /etc/hosts

## [qemu-0.1.0], [vbox-0.1.0] - 2021-10-01
### Added
- First CentOS 8.4 version

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-8.4/-/tree/qemu-0.1.0
[vbox-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-8.4/-/tree/vbox-0.1.0
[qemu-0.1.1]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-8.4/-/tree/qemu-0.1.1
[qemu-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-8.4/-/tree/qemu-0.2.0
[vbox-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/centos-8.4/-/tree/vbox-0.2.0
